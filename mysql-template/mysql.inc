public string GetClassName(TableSchema table)
{
    string name = table.Name.Replace("sdb_","");
    for (int i = 0; i <= 2; i++)
    {
        if(name.IndexOf('_')>-1)
        {
            string replace = name.Substring(name.IndexOf('_')+1, 1);
            name = name.Replace("_" + replace, replace.ToUpper());
        }
    }
    return GetUpperName(name);
}

public string GetSmallClassName(TableSchema table)
{
    return GetCamelCaseName(GetClassName(table));
}

public string GetFirstLetter(TableSchema table)
{
    string name = table.Name.Replace("sdb_","").Substring(0, 1);;
    return name;
}

public string GetCamelCaseName(string value)
{
 return value.Substring(0, 1).ToLower() + value.Substring(1);
}
public string GetUpperName(string value)
{
 return value.Substring(0, 1).ToUpper() + value.Substring(1);
}

public string GetAnnotation(ColumnSchema column,String excludeProperty)
{
  string result="";
  result=result+"\r  @JsonProperty";
  string description=Regex.Replace(column.Description,"（.*?）","");
  if(column.DataType==DbType.String)
  {
    result=result+"\r  @Length(message=\""+description+"最大长度不能超过"+column.Size/3+"\",max ="+column.Size/3+",groups = {Default.class,Save.class})";
  }

  string upperColName=GetColumnName(column);
  string colName=GetVariableName(upperColName);
  if(!column.AllowDBNull&&!excludeProperty.Contains(colName))
  {
    
    string notEmpty= column.DataType==DbType.String?"@NotEmpty":"@NotNull";
    result=result+"\r  "+notEmpty+"(message=\""+description+"不能为空\",groups = {Default.class,Save.class})";
  }

  return result;
}

public string GetPrimaryKeyType(TableSchema table)
{
 if (table.PrimaryKey != null)
 {
  if (table.PrimaryKey.MemberColumns.Count == 1)
  {
   return GetCSharpVariableType(table.PrimaryKey.MemberColumns[0]);
  }
  else
  {
   throw new ApplicationException("This template will not work on primary keys with more than one member column.");
  }
 }
 else
 {
  throw new ApplicationException("This template will only work on tables with a primary key.");
 }
}
public string GetCSharpVariableType(ColumnSchema column)
{
 switch (column.DataType)
 {
  case DbType.AnsiString: return "string";
  case DbType.AnsiStringFixedLength: return "string";
  case DbType.Binary: return "byte[]";
  case DbType.Boolean: return "Boolean";
  case DbType.Byte: return "byte";
  case DbType.Currency: return "Decimal";
  case DbType.Date: return "Date";
  case DbType.DateTime: return "Date";
  case DbType.Decimal: return "BigDecimal";
  case DbType.Double: return "Double";
  case DbType.Guid: return "Guid";
  case DbType.Int16: return "Integer";
  case DbType.Int32: return "Integer";
  case DbType.Int64: return "Long";
  case DbType.Object: return "Object";
  case DbType.SByte: return "Integer";
  case DbType.Single: return "Float";
  case DbType.String: return "String";
  case DbType.StringFixedLength: return "String";
  case DbType.Time: return "TimeSpan";
  case DbType.UInt16: return "Boolean";
  case DbType.UInt32: return "Integer";
  case DbType.UInt64: return "ulong";
  case DbType.VarNumeric: return "decimal";
  default:
  {
   return "__UNKNOWN__" + column.NativeType;
  }
 }
}

public string GetMySqlType(ColumnSchema column)
{
 switch (column.DataType)
 {
  case DbType.AnsiString: return "string";
  case DbType.AnsiStringFixedLength: return "string";
  case DbType.Binary: return "byte[]";
  case DbType.Boolean: return "bool";
  case DbType.Byte: return "byte";
  case DbType.Currency: return "decimal";
  case DbType.Date: return "Date";
  case DbType.DateTime: return "TIMESTAMP";
  case DbType.Decimal: return "decimal";
  case DbType.Double: return "double";
  case DbType.Guid: return "Guid";
  case DbType.Int16: return "Integer";
  case DbType.Int32: return "Integer";
  case DbType.Int64: return "BIGINT";
  case DbType.Object: return "object";
  case DbType.SByte: return "Integer";
  case DbType.Single: return "float";
  case DbType.String: return "varchar";
  case DbType.StringFixedLength: return "varchar";
  case DbType.Time: return "TimeSpan";
  case DbType.UInt16: return "Integer";
  case DbType.UInt32: return "Integer";
  case DbType.UInt64: return "ulong";
  case DbType.VarNumeric: return "decimal";
  default:
  {
   return "__UNKNOWN__" + column.NativeType;
  }
 }
}

public string GetVariableName(string name)
{
 return GetCamelCaseName(name);
}
public string GetColumnName(ColumnSchema column)
{
    string name = column.Name;
    name=name.Replace(name,name.Substring(0,1).ToUpper()+name.Substring(1));
    for (int i = 0; i <= 2; i++)
    {
        if(name.IndexOf('_')>-1)
        {
            string replace = name.Substring(name.IndexOf('_')+1, 1);
            name = name.Replace("_" + replace, replace.ToUpper());
        }
    }
    return name;
}

public string GetVariableDeclarationStatement(ColumnSchema column)
{
 string statement = "private ";
 //if(column.IsForeignKeyMember)
 //{
 // statement +=column.Name.Substring(0, column.Name.Length - 2)+" "+GetCamelCaseName(column.Name.Substring(0, column.Name.Length - 2));
 //}
// else
 //{
  statement += GetCSharpVariableType(column) + " " + GetVariableName(GetColumnName(column));
 //}
  
 string defaultValue = GetVariableDefaultValue(column);
 if (defaultValue != "")
 {
  statement += " = " + defaultValue;
 } 
 statement += ";"; 
 return statement;
}


public string GetPropertieDeclaration(ColumnSchema column)
{
 string statement = "public ";
 if(column.IsForeignKeyMember)
 {
  statement +=column.Name.Substring(0, column.Name.Length - 2)+" "+column.Name.Substring(0, column.Name.Length - 2);
 }
 else
 {
  statement += GetCSharpVariableType(column) + " " + column.Name;
 }
 return statement;
}
public string GetVariableDefaultValue(ColumnSchema column)
{
 switch (column.DataType)
 {
  case DbType.Guid:
  {
   return "Guid.Empty";
  }
  case DbType.AnsiString:
  case DbType.AnsiStringFixedLength:
  case DbType.String:
  case DbType.StringFixedLength:
  default:
  {
   return "";
  }
 }
}
public string MakeSingle(string name)
{
 Regex plural1 = new Regex("(?<keep>[^aeiou])ies$");
 Regex plural2 = new Regex("(?<keep>[aeiou]y)s$");
 Regex plural3 = new Regex("(?<keep>[sxzh])es$");
 Regex plural4 = new Regex("(?<keep>[^sxzhyu])s$");

 if(plural1.IsMatch(name))
  return plural1.Replace(name, "${keep}y");
 else if(plural2.IsMatch(name))
  return plural2.Replace(name, "${keep}");
 else if(plural3.IsMatch(name))
  return plural3.Replace(name, "${keep}");
 else if(plural4.IsMatch(name))
  return plural4.Replace(name, "${keep}");

 return name;
}